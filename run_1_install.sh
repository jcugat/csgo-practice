#!/bin/bash

# Run everything relative to this script path
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Download urls
STEAMCMD_URL="http://media.steampowered.com/client/steamcmd_linux.tar.gz"
METAMOD_URL="https://mms.alliedmods.net/mmsdrop/1.10/mmsource-1.10.7-git961-linux.tar.gz"
SOURCEMOD_URL="https://sm.alliedmods.net/smdrop/1.8/sourcemod-1.8.0-git6041-linux.tar.gz"
PRACTICEMODE_URL="https://github.com/splewis/csgo-practice-mode/releases/download/1.3.2/practicemode_1.3.2.zip"

# Install required packages
sudo dpkg --add-architecture i386
sudo apt-get update
sudo apt-get upgrade -y --no-install-recommends
sudo apt-get install -y --no-install-recommends \
    bsdtar \
    ca-certificates \
    dnsmasq \
    lib32gcc1 \
    lib32stdc++6 \
    net-tools \
    vim \
    wget

# Extract SteamCMD
mkdir -p ${BASE_DIR}/Steam
curl -sqL ${STEAMCMD_URL} | tar zxvf - -C ${BASE_DIR}/Steam

# Generate SteamCMD script
cat > ${BASE_DIR}/steamcmd_csgo_script.txt <<EOL
login anonymous
force_install_dir ${BASE_DIR}/server
app_update 740 validate
quit
EOL

# Download game files
${BASE_DIR}/Steam/steamcmd.sh +runscript ${BASE_DIR}/steamcmd_csgo_script.txt

# Install plugins
curl -sqL ${METAMOD_URL} | tar zxvf - -C ${BASE_DIR}/server/csgo/
curl -sqL ${SOURCEMOD_URL} | tar zxvf - -C ${BASE_DIR}/server/csgo/
curl -sqL ${PRACTICEMODE_URL} | bsdtar zxvf - -C ${BASE_DIR}/server/csgo/

# Last command: overwrite default files with ours
git reset --hard
