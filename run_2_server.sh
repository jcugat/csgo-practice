#!/bin/bash

# Run everything relative to this script path
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$#" -ne 0 ]; then
    # User supplied parameters, use them directly
    SERVER_ARGS="$@"
else
    # No parameters, use defaults
    SERVER_ARGS="-game csgo -steam_dir ${BASE_DIR}/Steam/ -steamcmd_script ${BASE_DIR}/steamcmd_csgo_script.txt"

    # Allow disabling autoupdate
    [ "${AUTOUPDATE}" != "0" ] && SERVER_ARGS="${SERVER_ARGS} -autoupdate"

    SERVER_ARGS="${SERVER_ARGS} -console -usercon +game_type 0 +game_mode 1 +mapgroup mg_active +map de_dust2"
fi

# Add extra parameters from environment variables
[ ! -z "${GSLT}" ]          && SERVER_ARGS="${SERVER_ARGS} +sv_setsteamaccount ${GSLT}"
[ ! -z "${RCON_PASSWORD}" ] && SERVER_ARGS="${SERVER_ARGS} +rcon_password \"${RCON_PASSWORD}\""

exec ${BASE_DIR}/server/srcds_run ${SERVER_ARGS}